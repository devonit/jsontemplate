/**
  Copyright Devon IT 2013
  Template system that uses a CTemplate format 
  template and a JSON structure to populate values
*/

#include <ctemplate/template.h>
#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <jansson.h>
#include <stdlib.h>

bool populate_dict( std::string key, json_t *value, ctemplate::TemplateDictionary *dict )
{
    if ( value == NULL )
    {
        std::cerr << "Null value " << key << std::endl;
        return false;
    }
    else if ( json_is_array( value ) )
    {
        for(int i = 0; i < json_array_size(value); i++)
        {
            json_t *data  = json_array_get(value, i);
            if ( !populate_dict( key, data, dict) )
            {
                return false;
            }
        }
        return true;
    }
    else if (json_is_integer( value ) )
    {
         dict->SetIntValue(key, json_integer_value( value ));
         return true;
    }
    else if (json_is_real( value ) )
    {
         dict->SetFormattedValue(key, "%f", json_real_value( value ));
         return true;
    }
    else if (json_is_boolean( value ) )
    {
         dict->SetFormattedValue(key, "%d", json_is_true(value));
         return true;
    }
    else if (json_is_string( value ) )
    {
        dict->SetValue(key, json_string_value( value ));
        return true;
    }
    else if ( json_is_object( value ) )
    {
        ctemplate::TemplateDictionary* sub_dict = dict->AddSectionDictionary(key);
        void *iter = json_object_iter(value);
        while ( iter != NULL)
        {
            if ( !populate_dict( json_object_iter_key(iter), json_object_iter_value(iter), sub_dict) )
            {
                return false;
            }

            iter = json_object_iter_next(value,iter);
        }

        return true;
    }
    else
    {
        std::cerr << "error: unsupported JSON value" << std::endl;
        return false;
    }
    return false;
}

bool load_json( char *fileName, ctemplate::TemplateDictionary *dict)
{
    json_error_t error;

    json_t *root = json_load_file(fileName, JSON_REJECT_DUPLICATES, &error);
    if(!root)
    {
        std::cerr <<  "json parse error: " << error.text << " on line " <<  error.line << std::endl;
	return false;
    }

    if(!json_is_object(root))
    {
        std::cerr << "error: object expected" << std::endl;
        return false;
    }

    const char *key;
    json_t *value;
    void *iter = json_object_iter(root);
    while ( iter != NULL)
    {
       const char *key = json_object_iter_key(iter);
       json_t *value = json_object_iter_value(iter);
       if ( !populate_dict( key, value, dict) )
       {
           return false;
       }
       iter = json_object_iter_next(root,iter); 
    }
    return true;
}

int main( int argc, char **argv )
{
    if ( argc != 4 )
    {
        std::cerr << "Usage: jsontemplate <template> <json> <output>" << std::endl;
        return 1;
    }

    std::string output_string;
    ctemplate::TemplateDictionary dict("root");
    if ( load_json( argv[2], &dict) )
    {
        if ( strncmp (getenv ("DEBUG_DICT") ,"1",1) == 0)
           dict.Dump(2);

        ctemplate::ExpandTemplate(argv[1], ctemplate::DO_NOT_STRIP, &dict, &output_string);
        std::ofstream file;
        file.open(argv[3]);
        file << output_string;
        file.close();
        return 0;
    }
    return 1;
}
