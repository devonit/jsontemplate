# CTemplate frontend with JSON data input.

A simple utility that will take an input json data file and expand a template with that data.  The templates are based on Google's CTemplate format.
The template language itself is documented at:
http://google-ctemplate.googlecode.com/svn/trunk/doc/reference.html

The JSON format must be an object at the top level, but after that any structure in the file will be exposed as values in the template.

Usage:

```
$ jsontemplate <template file> <json file> <output file>
```

Requirements:

 * [Jansson](http://www.digip.org/jansson)
 * [CTemplate](https://code.google.com/p/ctemplate)
 * gnu make / gcc

Build:

```
$ make && make install DEST_DIR=/path/to/destdir
```


