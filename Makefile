OBJ = main.cpp
CFLAGS=-I. $(pkg-config --cflags jansson) $(pkg-config --cflags libctemplate)
LIBS=$(pkg-config --libs jansson) $(pkg-config --libs libctemplate)

%.o: %.cpp
	$(CXX) -c -o $@ $< $(CFLAGS)

jsontemplate: $(OBJ)
	$(CXX) -o $@ $^ $(CFLAGS) $(LIBS)

test: jsontemplate
	./jsontemplate test.tmpl test.json test.out

clean:
	$(RM) *.o jsontemplate test.out

install: jsontemplate
	install -d $(DEST_DIR}/usr/bin
	install -m 755 jsontemplate $(DEST_DIR)/usr/bin/jsontemplate
